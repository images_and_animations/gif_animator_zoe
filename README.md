# GIF animator Zoe



## _Actually not only useful for GIFs..._
<div align='center'>
<img src="readme_files/Zoe_project_banner.gif" style="display: inline; border-width: 0px;" width=410px>
</div>
This repository contains a module to create animations based on an input folder 
with some images. The final animation can be exported either as an animated GIF or as 
a sequence of individual frames (i.e. each frame will be stored as PNG-file).



## Introduction video
The following image guides to a short video, showing some demonstrations and explains the key features 
of the animator in a visual appealing way.



[![IMAGE_ALT](https://img.youtube.com/vi/YK4Tzaxeuh8/0.jpg)](https://youtu.be/YK4Tzaxeuh8)

## Structure of a Zoe animation
Each loaded image is represented as one keyframe. After each keyframe a certain number of pause frames is inserted.
This pause frames are just duplicates of the current keyframe. 
If fading effect is enabled Zoe inserts so-called inbetweens (or fading images).

<div align='center'>
<img src="readme_files/structure_of_Zoe_animations.svg" style="display: inline; border-width: 0px;" width=1080px>
</div>

## Supported file formats
|                          | supported formats                                    |
|--------------------------|------------------------------------------------------|
| input file extensions    | .jpg (or .jpeg) <br>  .png                           |
| input image color format | grayscale (one color channel) <br> RGB <br> RGBA     |
| output format            | .gif (for animation)  <br> .png (for image sequence) |


## Adjustable parameters
The following table shows all parameter that can be changed by the user. For quick adjustments of the settings 
make use of the configuration file that is located in the folder '_config_'.


| parameter                     | explaination                                                                                                                                                                              |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `input_img_folder`            | Relative path to the folder with input images                                                                                                                                             |
| `output_height`               | Height of the output frames  <br>  <em>NOTE: will be scaled by parameter<em> `post_scaling_factor`                                                                                        |
| `output_width`                | Width of the output frames   <br>  <em>NOTE: will be scaled by parameter<em> `post_scaling_factor`                                                                                        |                                                       |
| `automatic_output_resolution` | Get the resolution of the first image in the animation as output resolution                                                                                                               |
| `post_scaling_factor`         | Scaling factor that will be applied to the output resolution                                                                                                                              |
| `letterboxing`                | If True, add horizontal bars to the image. <br> Otherwise it zooms into the image to fill the given output resolution                                                                     |
| `pillarboxing`                | If True, add vertical bars to the image. <br> Otherwise it zooms into the image to fill the given output resolution                                                                       |
| `frame_rate`                  | Speed of the animation sequence in FPS                                                                                                                                                    |
| `enable_fading`               | If True, activate fading effect                                                                                                                                                           |
| `fading_steps`                | Number of frames that are used for the fading effect                                                                                                                                      |
| `playback_mode`               | Currently 4 different playback modi are supported:  <br> 0 = play forward  <br> 1 = play forward and then backward <br>    2 = play backward   <br>    3 = play backward and then forward |
| `pause_first_image`           | Number of frames that will be inserted as pause for the first image of the sequence                                                                                                       |
| `pause_last_image`            | Number of frames that will be inserted as pause for the last image of the sequence                                                                                                        |
| `general_keyframe_pause`      | Number of frames that will be inserted as pause for each image of the sequence except for the first & last image                                                                          |
| `export_animation_as_gif`     | If True, export the created animation as GIF file                                                                                                                                         |
| `export_frames_as_pngs`       | If True, export the created animation as PNG file                                                                                                                                         |
| `output_folder_animation`     | Name of the export folder for the animated GIFs. <br> If folder does not already exist it will be created.                                                                                |
| `output_folder_frames`        | Name of the export folder for the frame sequence. <br> <em> NOTE: All files in this folder will be deleted with a rerun of the code <em>                                                  |
| `output_filename_stem`        | Beginning of the filename. <br> Depending on the type of export some other information can be added to this name automatically (e.g. index of the stored frame)                           |




## Setup and required libraries
I used Python 3.9 for this project.

The following libraries are necessary to run the repo. Additionally, I notate the specific library versions of my own setup (just in case it doesn't work with your installed library versions):
- opencv 4.6.0.66 (i.e. cv2)
- imageio 2.21.2
- Numpy 1.23.1
- Pandas 1.5.0
- Pillow 9.2.0 (i.e. PIL)
- PyYAML 6.0

## Running example code
To run a simple animation example from this repo just type into your terminal:
```bash
# Start in the project root directory
cd src
python create_img_sequence.py
```

When running the script, it uses a YAML file from folder "config" to set the parameter of the animator. 


## Create animation with your own images
If you want to create an animation with your own images you have to do these steps:
1. collect all images that you want to use for a single animation and store them in a new folder (NOTE: currently only **jpg/jpeg/png** images and **grayscale/RGB/RGBA** color formats are supported!)


2. copy this folder into the folder **'input_imgs'**


3. open the animator_settings.yaml file in the folder **'config'** and adjust the settings:
    1. for parameter **input_folder** you have to insert the name of the image folder which you have copied into **'input_imgs'**. Let's assume the inserted folder has the name "my_own_images" then you have to write **input_imgs/my_own_images/**
    2. adjust the parameter **frame_rate, fading, fading_steps** to your own preferences
    3. (optional) change some of the other parameter e.g. the name of the output file or the output resolution (output_width & output_height)


4. save new settings in the yaml config file


5. run script as described in the section **Running example code** (see above)


6. the created GIF animation will be stored in the folder **saved_animation** respectively the frame sequence (if parameter **export_frames_as_pngs** is set to True) will be saved in folder **saved_frame_sequence**


## Accessing other test images
I also provide some other example images (real photos) to test the animator which you can download here:
https://drive.google.com/drive/folders/1gB-hFJh2peFCvQ-veS-fTcgljwzDOrP2?usp=sharing


## Workflow of the animator
If you are interested in the main processing steps & its order you should take a look at this table.


|     | working step                       | further details                                                                                                                                                                                                                            |
|-----|------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | load images from input folder      | Only the supported file extensions will be added to the file list <br>  All other files inside the folder will be ignored.                                                                                                                 |
| 2   | convert images to one color format | Currently the code internally works with BGRA (the recommended RGBA format for the opencv library).                                                                                                                                        |
| 3   | create background image            | The background is used for letterboxing & pillarboxing.                                                                                                                                                                                    |
| 4   | adjustment of each image           | In case the images don't match the size and/or aspect ratio of the selected output resolution some scaling will be applied. After that the animator either perform a center crop or insert black bars (i.e. letterboxing or pillarboxing). |
| 5   | post scaling                       | This scaling directly change the output resolution of the frames if the factor is not equal to 1.0! It's intended to be a very fast & easy way to adjust the size of the frame without change the output height & output width.            |
| 6   | create frame sequence              | Pause frames and fading images will be computed & added to the sequence. Then the sequence will be adapted to the playback mode (play forward and/or backward).                                                                            |
| 7   | export phase                       | Here the created animation will be stored either as one GIF file and/or as series of PNG files.                                                                                                                                            |
                                                                                                                                                                                 |

