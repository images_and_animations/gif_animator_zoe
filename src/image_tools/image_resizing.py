import cv2

from image_tools import image_basics


######################################################################################################################
# resize image
#
# NOTE: in image_basics you can find the function 'resize_img' that allow a simple resize of an image
#       this was done to prevent a circular import of the modules 'image_basics' and 'image_resizing'


######################################################################################################################
#  adjust image to other resolution & format
#
def adjust_image_to_given_shape(img, new_height=1080, new_width=1920, letterboxing=True, pillarboxing=True):
    num_of_channels = image_basics.get_number_of_color_channels(img)
    background_img = image_basics.create_empty_image(new_height, new_width, color_channels=3, init_value=0)
    background_img = image_basics.convert_to_other_color_format(background_img, num_of_channels)

    adjusted_img = adjust_image_to_background_image(img, background_img, letterboxing, pillarboxing)
    return adjusted_img


def adjust_image_to_background_image(img, background_img, letterboxing=True, pillarboxing=True):
    img = ensure_image_has_same_channel_number_as_background(img, background_img)
    img_aspect_ratio = image_basics.get_aspect_ratio(img)
    background_aspect_ratio = image_basics.get_aspect_ratio(background_img)

    # compare aspect ratios & perform either letterboxing, pillarboxing or image cropping:
    if background_aspect_ratio < img_aspect_ratio:
        if letterboxing:
            scaled_img = apply_letterbox_adjustment(img, background_img)
        else:
            scaled_img = apply_image_crop_for_more_vertical_background(img, background_img)

    else:
        if pillarboxing:
            scaled_img = apply_pillarbox_adjustment(img, background_img)
        else:
            scaled_img = apply_image_crop_for_more_horizontal_background(img, background_img)

    adjusted_img = insert_image_into_center_of_another_image(scaled_img, background_img)
    return adjusted_img


def ensure_image_has_same_channel_number_as_background(img, background_img):
    img_num_channels = image_basics.get_number_of_color_channels(img)
    background_num_channels = image_basics.get_number_of_color_channels(background_img)
    if img_num_channels == background_num_channels:
        return img
    else:
        new_img = image_basics.convert_to_other_color_format(img, background_num_channels)
        return new_img


def apply_image_crop_for_more_vertical_background(img, background_img):
    bg_height, _ = image_basics.get_height_and_width_of_image(background_img)
    scaled_img = scale_image_to_certain_height(img, bg_height)
    return scaled_img


def apply_image_crop_for_more_horizontal_background(img, background_img):
    _, bg_width = image_basics.get_height_and_width_of_image(background_img)
    scaled_img = scale_image_to_certain_width(img, bg_width)
    return scaled_img


def apply_letterbox_adjustment(img, background_img):
    _, bg_width = image_basics.get_height_and_width_of_image(background_img)
    scaled_img = scale_image_to_certain_width(img, bg_width)
    return scaled_img


def apply_pillarbox_adjustment(img, background_img):
    bg_height, _ = image_basics.get_height_and_width_of_image(background_img)
    scaled_img = scale_image_to_certain_height(img, bg_height)
    return scaled_img


######################################################################################################################
#  insert image into the center of a background image
#
def insert_image_into_center_of_another_image(img, background):
    """ insert an image into another image (background) at the center position """
    new_img = background.copy()
    foreground = img.copy()

    h_img, w_img = image_basics.get_height_and_width_of_image(img)
    h_bg, w_bg = image_basics.get_height_and_width_of_image(background)
    h_diff = h_bg - h_img
    w_diff = w_bg - w_img

    if h_diff >= 0:
        h_start = h_diff // 2
        h_end = h_start + h_img

    else:
        h_start = 0
        h_end = h_bg
        fg_h_start = (-1 * h_diff) // 2
        fg_h_end = fg_h_start + h_bg
        foreground = foreground[fg_h_start:fg_h_end, :, :]   # central crop of the foreground image

    if w_diff >= 0:
        w_start = w_diff // 2
        w_end = w_start + w_img

    else:
        w_start = 0
        w_end = w_bg
        fg_w_start = (-1 * w_diff) // 2
        fg_w_end = fg_w_start + w_bg
        foreground = foreground[:, fg_w_start:fg_w_end, :]   # central crop of the foreground image

    new_img[h_start:h_end, w_start:w_end, :] = foreground
    return new_img


######################################################################################################################
#  adjust image to a certain width or height
#
def scale_image_to_certain_width(img, output_width):
    """ scale image (with color channels) to a given width
        NOTE: it round up the scaling factor & crop image to the correct width value """
    new_img = scale_one_image_dimension_to_given_value(img, output_width, adjust_width=True)
    return new_img


def scale_image_to_certain_height(img, output_height):
    """ scale image (with color channels) to a given width
        NOTE: it round up the scaling factor & crop image to the correct width value """
    new_img = scale_one_image_dimension_to_given_value(img, output_height, adjust_width=False)
    return new_img


def scale_one_image_dimension_to_given_value(img, new_dimension_value, adjust_width=True):
    """ scale either the width or the height of the image to a given value
        NOTE A: it round up the scaling factor & crop image to the correct reference value
        NOTE B: it will not adjust the other dimensions of the image! """
    shape_idx = 1 if adjust_width else 0
    scaling_factor = new_dimension_value / img.shape[shape_idx]
    scaling_factor = round(scaling_factor, 6) + 0.000001
    scaled_img = image_basics.resize_img(img, scaling_factor)
    if adjust_width:
        return scaled_img[:, :new_dimension_value, :]
    else:
        return scaled_img[:new_dimension_value, :, :]
