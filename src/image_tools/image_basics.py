from pathlib import Path

import cv2
import numpy as np
from PIL import Image


SIZE_OF_PREVIEW_WINDOW=600

######################################################################################################################
# find image files in folder
#
def find_all_images_in_folder(path, file_extension_list=['png', 'jpeg', 'jpg']):
    search_ext_list = ['.' + elem.lower() for elem in file_extension_list]
    image_file_list = []

    for file in path.iterdir():
        for file_ext in search_ext_list:
            if file.suffix == file_ext:
                image_file_list.append(file)
    print_all_elements_from_file_list(image_file_list)
    return image_file_list


def print_all_elements_from_file_list(image_file_list):
    print(f'\n the list contains {len(image_file_list)  :>4} files:')
    for idx, file in enumerate(image_file_list):
        print(f'  +++ file {idx:04}:   {file}')
    print()


######################################################################################################################
# load images
#
def load_any_image(img_path, show_img=False, convert_to_cv2_format=True):
    """  load an image with PIL  &  export it either as  cv2  or  PIL  format """
    img_pil = Image.open(img_path)  # load image in PIL format

    if convert_to_cv2_format:
        img_cv2 = convert_PIL_image_to_cv2_image(img_pil)  # convert image to cv2 format
        img_cv2 = ensure_that_image_has_three_dimensions(img_cv2)
        if show_img:
            show_image(img_cv2)
        return img_cv2

    else:
        img_pil = ensure_that_image_has_three_dimensions(img_pil)
        if show_img:
            Image.show(img_pil)
        return img_pil


def load_img(img_path, no_channels, show_img=False):
    if no_channels == 4:
        img = cv2.imread(img_path, cv2.IMREAD_UNCHANGED)
    elif no_channels == 3:
        img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    elif no_channels == 1:
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    elif no_channels == -1:
        img = cv2.imread(img_path, cv2.IMREAD_ANYDEPTH)     # choose this if you don't know the exact color depth of your image
    else:
        raise Exception('\n   Number of channels is not 1, 3 or 4. Please check the dimensions of the image! \n\n')
    if show_img: show_image(img)
    img = ensure_that_image_has_three_dimensions(img)
    return img


######################################################################################################################
# convert image between different formats
#
def convert_cv2_image_to_PIL_image(img_cv):
    img_rgb = cv2.cvtColor(img_cv, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img_rgb)
    return im_pil


def convert_PIL_image_to_cv2_image(img_pil, print_info=False):
    """  convert image with PIL format into a cv2-format  &  set the order of color channels correctly
      NOTE: output will be either in BGR  or  BGRA format """
    img = np.asarray(img_pil)

    if print_info:
        print(f'  load img has the shape = {img.shape}      (with {img.ndim = }) \n')

    if img.ndim == 2:                                   # for gray scale images
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    elif img.shape[2] == 3:                             # for RGB images
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    elif img.shape[2] == 4:                             # for RGBA images
        img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGRA)

    return img


def convert_to_other_color_format(img, output_number_of_channels=3):
    """This module can convert between 3 different color modes:
        grayscale, BGR and BGRA  """
    cur_number_of_channels = get_number_of_color_channels(img)
    new_img = img.copy()

    # special case: grayscale with alpha will be converted to RGBA format
    if cur_number_of_channels == 2:
        new_img = convert_grayscale_with_alpha_to_rgba(new_img)
        cur_number_of_channels = 4

    if cur_number_of_channels == output_number_of_channels:
        return new_img

    elif cur_number_of_channels == 1 and output_number_of_channels == 3:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_GRAY2BGR)

    elif cur_number_of_channels == 1 and output_number_of_channels == 4:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_GRAY2BGRA)

    elif cur_number_of_channels == 3 and output_number_of_channels == 1:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_BGR2GRAY)

    elif cur_number_of_channels == 3 and output_number_of_channels == 4:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_BGR2BGRA)

    elif cur_number_of_channels == 4 and output_number_of_channels == 1:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_BGRA2GRAY)

    elif cur_number_of_channels == 4 and output_number_of_channels == 3:
        new_img = cv2.cvtColor(new_img, cv2.COLOR_BGRA2BGR)

    else:
        raise ValueError(f'this type of conversion is currently not supported:  '
                         f'from {cur_number_of_channels} to {output_number_of_channels} color channels')
    new_img = ensure_that_image_has_three_dimensions(new_img)
    return new_img


def convert_grayscale_with_alpha_to_rgba(img_with_two_channels):
    color_arr = img_with_two_channels[:, :, 0]
    alpha_arr = img_with_two_channels[:, :, 1]

    new_img = cv2.cvtColor(color_arr, cv2.COLOR_GRAY2BGR)
    new_img = np.dstack([new_img, alpha_arr])  # will stack arrays in sequence depth wise (along 3. dimension)
    return new_img


def ensure_that_image_has_three_dimensions(img):
    num_channels = get_number_of_color_channels(img)
    if num_channels == 0:
        img = img[..., np.newaxis]              # np.newaxis is an alias for None
    elif num_channels < 0 or num_channels > 4:
        raise ValueError(f'The current image has  {num_channels}  color channels - which is not supported yet!')
    return img


######################################################################################################################
# show image
#
def show_image(img, pause_time=1000, scaling_factor=None, size_of_preview_window=SIZE_OF_PREVIEW_WINDOW):
    img_copy = img.copy()
    if scaling_factor is not None:
        img_copy = resize_img(img_copy, scaling_factor)
    else:
        img_copy = resize_img_automatically_for_show(img_copy, size_of_preview_window)
    cv2.imshow("", img_copy)
    cv2.waitKey(pause_time)


def resize_img(img, scaling_factor):
    width = int(img.shape[1] * scaling_factor)
    height = int(img.shape[0] * scaling_factor)
    resized_img = cv2.resize(img, (width, height), interpolation=cv2.INTER_AREA)

    resized_img = ensure_that_image_has_three_dimensions(resized_img)
    return resized_img


def resize_img_automatically_for_show(img, size_of_preview_window=700):
    new_img = img.copy()
    scaling_factor = get_scaling_factor_for_show(img, size_of_preview_window)
    new_img = resize_img(new_img, scaling_factor)
    return new_img


def get_scaling_factor_for_show(img, size_of_preview_window=700):
    largest_dim_value = get_largest_dimension_value_of_image(img)
    scaling_factor = size_of_preview_window / largest_dim_value
    return scaling_factor


######################################################################################################################
# get basic metric values of the image
#
def get_largest_dimension_value_of_image(img):
    height, width = get_height_and_width_of_image(img)
    largest_dim_value = width if width >= height else height
    return largest_dim_value


def get_smallest_dimension_value_of_image(img):
    height, width = get_height_and_width_of_image(img)
    smallest_dim_value = width if width <= height else height
    return smallest_dim_value


def get_height_and_width_of_image(img):
    height = img.shape[0]
    width = img.shape[1]
    return height, width


def get_aspect_ratio(img):
    aspect_ratio = img.shape[1] / img.shape[0]      # aspect ratio = width / height
    return aspect_ratio


def get_number_of_color_channels(img):
    try:
        num_channels = img.shape[2]
    except IndexError:
        num_channels = 0                # NOTE: possible future error source
    return num_channels


######################################################################################################################
# store image
#
def store_img(img, filename, img_path, file_extension='.png'):
    # NOTE: img_path can here only be a relative path!!!
    # path = img_path + filename + file_extension
    if isinstance(img_path, str):
        #print(f'img_path is a string ')
        img_path = Path(img_path)
    elif isinstance(img_path, type(Path())):
        print()
        #print(f'img_path is a Path object ')
    else:
        raise ValueError('"img_path" has to be a string or a Path object!')
    filename += file_extension
    path_to_save_location = img_path / Path(filename)
    path_to_save_location = str(path_to_save_location)
    cv2.imwrite(path_to_save_location, img)
    #cv2.imwrite(img_path + filename + file_extension, img)


######################################################################################################################
# create empty image
#
def create_empty_image(height=1080, width=1920, color_channels=3, init_value=0):
    ''' create an empty (black) image with a certain given shape'''
    empty_img = np.ones((height, width, color_channels), np.uint8) * init_value
    return empty_img

######################################################################################################################
# create empty image
#
def create_simple_blend_of_two_images(foreground, background, alpha_for_foreground):
    """
    :param foreground:
    :param background:
    :param alpha_for_foreground:
    :return: new_img = alpha * foreground + (1 - alpha) * background
    """
    if alpha_for_foreground < 0 or alpha_for_foreground > 1:
        print(' \n\n  {alpha_for_foreground = }')
        raise ValueError('value of alpha_for_foreground is not inside the designated value range of [0.0; 1.0]')

    beta = 1.0 - alpha_for_foreground
    new_img = cv2.addWeighted(foreground, alpha_for_foreground, background, beta, gamma=0)
    return new_img
