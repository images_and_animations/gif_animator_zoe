from pathlib import Path

from animation_tools.animation_sequence import AnimationSequence
from animation_tools.gif_animator import GifAnimator
from image_tools import image_basics
from image_tools import image_resizing
from utils import path_utils

PROCESSING_NUM_CHANNELS = 4
FRAME_EXPORT_TYPE = 'png'
INPUT_FILE_EXTENSIONS = ['jpg', 'jpeg', 'png']


class ImageSequence:
    def __init__(self,
                 input_img_folder,

                 output_height=800,
                 output_width=800,
                 automatic_output_resolution=False,
                 post_scaling_factor=0.9,

                 letterboxing=False,
                 pillarboxing=False,

                 frame_rate=10,
                 enable_fading=True,
                 fading_steps=3,

                 playback_mode=3,

                 pause_first_image=2,
                 pause_last_image=2,
                 general_keyframe_pause=2,

                 export_animation_as_gif=True,
                 export_frames_as_pngs=True,
                 output_folder_animation='saved_animation',
                 output_folder_frames='saved_frame_sequence',
                 output_filename_stem='my_animation_001'
                 ):
        self.input_img_folder = input_img_folder

        self.output_height = output_height
        self.output_width = output_width    # 1920
        self.automatic_output_resolution = automatic_output_resolution    # use the resolution of the first image when set to True
        self.post_scaling_factor = post_scaling_factor

        self.letterboxing=letterboxing
        self.pillarboxing=pillarboxing

        self.frame_rate = frame_rate
        self.enable_fading = enable_fading
        self.fading_steps = fading_steps

        self.playback_mode = playback_mode

        self.pause_first_image = pause_first_image
        self.pause_last_image = pause_last_image
        self.general_keyframe_pause = general_keyframe_pause

        self.export_animation_as_gif = export_animation_as_gif
        self.export_frames_as_pngs = export_frames_as_pngs
        self.output_folder_animation = output_folder_animation
        self.output_folder_frames = output_folder_frames
        self.output_filename_stem = output_filename_stem

        self.all_found_files = []
        self.img_name_list = []
        self.img_list = []
        self.background_img = None
        self.animated_sequence = []
        self.display_time_by_frame_rate = None

    # --------------------------------------------------------------------------------------------------------
    # adapt display time to user defined frame rate
    #
    def compute_display_time_from_frame_rate(self):
        self.display_time_by_frame_rate = int(1000 / self.frame_rate)

    # --------------------------------------------------------------------------------------------------------
    # create output folder if necessary  &  delete all exported frame files from previous export
    #
    def create_output_folder_for_animation(self):
        path_utils.create_folder_in_cwd(self.output_folder_animation)

    def create_output_folder_for_frames(self):
        path_utils.delete_folder_in_cwd(self.output_folder_frames, ignore_if_folder_does_not_exist=False)
        path_utils.create_folder_in_cwd(self.output_folder_frames)

    # --------------------------------------------------------------------------------------------------------
    # find & load image files
    #
    def find_all_files_in_given_directory(self):
        path_to_imgs = Path(self.input_img_folder)
        if not path_to_imgs.exists():
            raise ValueError('the given directory does not exist!')
        else:
            self.all_found_files = sorted(file for file in path_to_imgs.iterdir() if file.is_file())
            if not self.all_found_files:
                raise ValueError('Could not find any files in this directory at all!')

    def find_all_images_in_given_file_list(self):
        for file in self.all_found_files:
            for supported_ext in INPUT_FILE_EXTENSIONS:
                cur_ext = str(file.suffix)
                cur_ext = cur_ext.replace('.', '').lower()      # remove '.' from suffix & convert to lower case
                if cur_ext == supported_ext.lower():
                    self.img_name_list.append(file)
        print(f'\nthe following {len(self.img_name_list):>4} images   will be used for the animation:')
        [print(f'  - {i}') for i in self.img_name_list]

    def load_content_of_images(self):
        for filename in self.img_name_list:
            img = image_basics.load_any_image(filename, show_img=False, convert_to_cv2_format=True)
            image_basics.show_image(img, pause_time=self.display_time_by_frame_rate)
            self.img_list.append(img)

    # --------------------------------------------------------------------------------------------------------
    # image preprocessing
    #
    def convert_all_images_to_rgba_format(self):
        for idx, img in enumerate(self.img_list):
            img = image_basics.convert_to_other_color_format(img, output_number_of_channels=PROCESSING_NUM_CHANNELS)
            self.img_list[idx] = img
            #print(f'  ~ shape of img = {self.img_list[idx].shape}')
            #image_basics.show_image(img, pause_time=2000)

    def check_output_resolution_settings(self):
        if self.automatic_output_resolution:
            first_img = self.img_list[0]
            self.output_height, self.output_width = image_basics.get_height_and_width_of_image(first_img)

    def create_black_background_image(self):
        img = image_basics.create_empty_image(self.output_height, self.output_width, color_channels=3, init_value=0)
        self.background_img = image_basics.convert_to_other_color_format(img, PROCESSING_NUM_CHANNELS)

    def adjust_size_of_all_images(self):
        for idx, img in enumerate(self.img_list):
            img = image_resizing.adjust_image_to_background_image(img, self.background_img,
                                                                        self.letterboxing,
                                                                        self.pillarboxing)
            self.img_list[idx] = img

    def apply_post_scaling(self):
        for idx, img in enumerate(self.img_list):
            img = image_basics.resize_img(img, self.post_scaling_factor)
            self.img_list[idx] = img

    def compute_animation(self):
        animation = AnimationSequence(keyframe_list=self.img_list,

                                      enable_fading=self.enable_fading,
                                      fading_steps=self.fading_steps,

                                      playback_mode=self.playback_mode,

                                      pause_first_image=self.pause_first_image,
                                      pause_last_image=self.pause_last_image,
                                      general_keyframe_pause=self.general_keyframe_pause)
        animation.create_full_animation_by_standard_mode()
        self.animated_sequence = animation.final_frame_sequence

    # --------------------------------------------------------------------------------------------------------
    # export animation & frames
    #
    def export_all_frames_of_animation(self):
        if self.export_frames_as_pngs:
            for idx, frame in enumerate(self.animated_sequence):
                filename = self.output_filename_stem + '_' + str(idx).zfill(4)
                image_basics.store_img(frame, filename, self.output_folder_frames, '.' + FRAME_EXPORT_TYPE)

    def export_animation_sequence_as_gif(self):
        if self.export_animation_as_gif:
            gif_animation_writer = GifAnimator(frame_list=self.animated_sequence,
                                               output_folder=self.output_folder_animation,
                                               output_filename=self.output_filename_stem,
                                               frame_rate=self.frame_rate)
            gif_animation_writer.export_animation_as_gif()

    # --------------------------------------------------------------------------------------------------------
    # create sequence:
    #
    def show_animated_sequence(self):
        for frame in self.animated_sequence:
            image_basics.show_image(frame, pause_time=self.display_time_by_frame_rate)

    def create_sequence(self):
        self.compute_display_time_from_frame_rate()
        self.create_output_folder_for_animation()
        self.create_output_folder_for_frames()
        self.find_all_files_in_given_directory()
        self.find_all_images_in_given_file_list()
        self.load_content_of_images()
        self.convert_all_images_to_rgba_format()
        self.check_output_resolution_settings()
        self.create_black_background_image()
        self.adjust_size_of_all_images()
        self.apply_post_scaling()
        self.compute_animation()
        self.show_animated_sequence()
        self.export_animation_sequence_as_gif()
        self.export_all_frames_of_animation()
