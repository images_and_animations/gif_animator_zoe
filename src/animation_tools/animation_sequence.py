from animation_tools import fading_curves
from image_tools import image_basics


class AnimationSequence:
    def __init__(self,
                 keyframe_list,

                 enable_fading=False,
                 fading_steps=3,

                 playback_mode=0,

                 pause_first_image=1,
                 pause_last_image=1,
                 general_keyframe_pause=0
                 ):
        self.keyframe_list = keyframe_list

        self.enable_fading = enable_fading
        self.fading_steps = fading_steps

        self.playback_mode = playback_mode

        self.pause_first_image = pause_first_image
        self.pause_last_image = pause_last_image
        self.general_keyframe_pause = general_keyframe_pause

        self.num_keyframes = len(self.keyframe_list)

        self.start_frame_sequence = []
        self.mid_frame_sequence = []
        self.end_frame_sequence = []

        self.final_frame_sequence = []
        self.pause_frame_list = []
        self.fading_value_list = []
        self.inbetween_list = []

    def create_pause_frame_list_by_standard_mode(self):
        self.pause_frame_list = [self.general_keyframe_pause] * len(self.keyframe_list)
        self.pause_frame_list[0] = self.pause_first_image
        self.pause_frame_list[-1] = self.pause_last_image

    def create_fading_curve(self):
        self.fading_value_list = fading_curves.get_curve_values(num_intermediate_bins=self.fading_steps,
                                                                curve_mode=0,
                                                                skip_start_and_end_value=True)

    def create_inbetweens_for_current_keyframe(self, keyframe_idx):
        cur_inbetweens = []
        if not self.enable_fading:
            self.inbetween_list.append(cur_inbetweens)

        elif keyframe_idx == (self.num_keyframes - 1):
            self.inbetween_list.append(cur_inbetweens)

        else:
            cur_keyframe = self.keyframe_list[keyframe_idx]
            next_keyframe = self.keyframe_list[keyframe_idx + 1]

            for alpha in self.fading_value_list:
                fading_img = image_basics.create_simple_blend_of_two_images(cur_keyframe, next_keyframe, alpha)
                cur_inbetweens.append(fading_img)
            self.inbetween_list.append(cur_inbetweens)

    def create_inbetween_list_for_all_keyframes(self):
        for idx in range(self.num_keyframes):
            self.create_inbetweens_for_current_keyframe(idx)

    def add_current_keyframe_to_mid_frame_sequence(self, keyframe_idx):
        cur_keyframe = self.keyframe_list[keyframe_idx]
        self.mid_frame_sequence.append(cur_keyframe)

    def add_current_pause_frames_to_mid_frame_sequence(self, keyframe_idx):
        cur_keyframe = self.keyframe_list[keyframe_idx]
        for i in range(self.pause_frame_list[keyframe_idx]):
            self.mid_frame_sequence.append(cur_keyframe)

    def add_current_inbetweens_to_mid_frame_sequence(self, keyframe_idx):
        cur_inbetweens = self.inbetween_list[keyframe_idx]
        for inbetween in cur_inbetweens:
            self.mid_frame_sequence.append(inbetween)

    def create_start_frame_sequence_by_standard_mode(self):
        first_keyframe = self.keyframe_list[0]
        self.start_frame_sequence.append(first_keyframe)
        for i in range(self.pause_frame_list[0]):
            self.start_frame_sequence.append(first_keyframe)

    def create_end_frame_sequence_by_standard_mode(self):
        last_keyframe = self.keyframe_list[-1]
        self.end_frame_sequence.append(last_keyframe)
        for i in range(self.pause_frame_list[-1]):
            self.end_frame_sequence.append(last_keyframe)

    def create_mid_frame_sequence_by_standard_mode(self):
        self.add_current_inbetweens_to_mid_frame_sequence(keyframe_idx=0)
        for idx in range(1, self.num_keyframes - 1):
            self.add_current_keyframe_to_mid_frame_sequence(idx)
            self.add_current_pause_frames_to_mid_frame_sequence(idx)
            self.add_current_inbetweens_to_mid_frame_sequence(idx)

    def merge_all_sequences_to_full_animation(self):
        reversed_mid_frame_sequence = self.mid_frame_sequence[:]
        reversed_mid_frame_sequence.reverse()

        if self.playback_mode==0:
            # playback: forward
            self.final_frame_sequence = self.start_frame_sequence[:]
            self.final_frame_sequence.extend(self.mid_frame_sequence)
            self.final_frame_sequence.extend(self.end_frame_sequence)

        elif self.playback_mode==1:
            # playback: forward & then backward
            self.final_frame_sequence = self.start_frame_sequence[:]
            self.final_frame_sequence.extend(self.mid_frame_sequence)
            self.final_frame_sequence.extend(self.end_frame_sequence)
            self.final_frame_sequence.extend(reversed_mid_frame_sequence)

        elif self.playback_mode==2:
            # playback: backward
            self.final_frame_sequence = self.end_frame_sequence[:]
            self.final_frame_sequence.extend(reversed_mid_frame_sequence)
            self.final_frame_sequence.extend(self.start_frame_sequence)

        elif self.playback_mode==3:
            # playback: backward & then forward
            self.final_frame_sequence = self.end_frame_sequence[:]
            self.final_frame_sequence.extend(reversed_mid_frame_sequence)
            self.final_frame_sequence.extend(self.start_frame_sequence)
            self.final_frame_sequence.extend(self.mid_frame_sequence)

        else:
            raise ValueError('currently only the playback mode 0, 1, 2 and 3 are supported!')

    def create_full_animation_by_standard_mode(self):
        self.create_pause_frame_list_by_standard_mode()
        self.create_fading_curve()
        self.create_inbetween_list_for_all_keyframes()
        self.create_start_frame_sequence_by_standard_mode()
        self.create_end_frame_sequence_by_standard_mode()
        self.create_mid_frame_sequence_by_standard_mode()
        self.merge_all_sequences_to_full_animation()


