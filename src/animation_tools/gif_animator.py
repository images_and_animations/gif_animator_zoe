from pathlib import Path

import cv2
import imageio

from image_tools import image_basics
from utils import path_utils


class GifAnimator:

    def __init__(self,
                 frame_list,
                 output_folder,
                 output_filename,
                 frame_rate
                 ):
        self.frame_list = frame_list
        self.output_folder = output_folder
        self.output_filename = output_filename
        self.frame_rate = frame_rate

    def create_output_folder_if_necessary(self):
        path_utils.create_folder_in_cwd(self.output_folder)

    def create_gif_writer(self):
        filename = self.output_filename + '.gif'
        path_to_file = self.output_folder / Path(filename)
        self.gif_writer = imageio.get_writer(path_to_file, mode='I', fps=self.frame_rate)

    def add_frames_to_gif_writer(self):
        """ export frame_list as gif animation by using the imageio writer """

        num_frames = len(self.frame_list) - 1                           # NOTE:  we subtract 1 from the actual length!
        print(f' \n\nexport the list with frames as GIF animation: ')
        for idx, frame in enumerate(self.frame_list):

            frame = image_basics.convert_to_other_color_format(frame, output_number_of_channels=3)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)              # finally convert image from BGR to RGB format
            print(f'  - write frame {idx:3} of {num_frames:3}',
                  f'    with the shape:  {frame.shape}')
            self.gif_writer.append_data(frame)
        print('\n')

    def export_animation_as_gif(self):
        self.create_output_folder_if_necessary()
        self.create_gif_writer()
        self.add_frames_to_gif_writer()
