import math


def calculate_adjusted_cosine_value(x):
    """ is based on a cosine function but shifted & with an offset! """
    adjusted_arg = (x * math.pi) + math.pi
    return (math.cos(adjusted_arg) + 1) * 0.5


def calculate_linear_value(x, m=1, n=0):
    return m * x + n


def get_curve_values(num_intermediate_bins=3, curve_mode=0, skip_start_and_end_value=True):
    """
    :param num_intermediate_bins:
    :param curve_mode:                   0 = linear,  1 = cosine-ish
    :param skip_start_and_end_value:    to exclude start & end (i.e. 0.0 & 1.0) from the fading curve
    :return:
    """
    curve_values = []
    step_size = 1.0 / (num_intermediate_bins + 1)
    bin_positions = [step_size * i for i in range(num_intermediate_bins + 2)]

    for cur_bin in bin_positions:
        if curve_mode == 0:
            y = calculate_linear_value(cur_bin, m=1, n=0)
        elif curve_mode == 1:
            y = calculate_adjusted_cosine_value(cur_bin)
        else:
            raise ValueError('the selected "curve_mode" is currently not supported')
        curve_values.append(y)

    if skip_start_and_end_value:
        curve_values = curve_values[1:-1]
    curve_values.reverse()
    return curve_values


if __name__ == '__main__':
    curve_value_list = get_curve_values(num_intermediate_bins=3, curve_mode=0)
    print()
