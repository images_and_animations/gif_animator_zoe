from animation_tools.image_sequence import ImageSequence
from utils import utils_config


def main():
    path_to_config_file = './config/img_sequence_settings.yaml'
    config_params = utils_config.load_config_file(path_to_config_file,
                                                  print_params=True)
    img_seq = ImageSequence(**config_params)
    img_seq.create_sequence()


if __name__ == '__main__':
    main()
