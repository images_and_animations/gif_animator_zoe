import os
from pathlib import Path


#----------------------------------------------------------------------------------------------------------------------
# create folder
#
def create_folder_in_cwd(folder_name):
    new_dir = Path(folder_name)
    if not new_dir.exists():    # always check is dir already exists! Otherwise it could cause errors
        new_dir.mkdir()


def create_multiple_folders_in_cwd_with_same_name_stem(folder_name_stem, num_folders=3, placeholders=3):
    for i in range(num_folders):
        folder_name = folder_name_stem + '_' + str(i).zfill(placeholders)
        create_folder_in_cwd(folder_name)


#----------------------------------------------------------------------------------------------------------------------
# delete folder
#
def delete_folder_in_cwd(folder_name, ignore_if_folder_does_not_exist=False):
    new_dir = Path(folder_name)
    if not new_dir.exists():        # always check is dir already exists! Otherwise it could cause errors
        if ignore_if_folder_does_not_exist:
            return None
        else:
            raise ValueError('the given Path does not exist!')

    all_files_in_dir, all_folders_in_dir = find_all_files_and_directories_in_a_folder(new_dir)
    if all_folders_in_dir:
        raise ValueError('given folder can not be deleted because it contains one or more subfolders!')
    else:
        for file in all_files_in_dir:
            file.unlink()
        new_dir.rmdir()
        return None


#----------------------------------------------------------------------------------------------------------------------
# find files and/or directories
#
def find_all_files_with_a_specific_extension_in_a_directory(path_obj, file_extension='*',
                                                            include_all_subdirectories=False):
    if not path_obj.exists():
        raise ValueError('the given Path does not exist!')

    if include_all_subdirectories:
        found_files = sorted(path_obj.glob('**/*.' + file_extension))
    else:
        found_files = sorted(path_obj.glob('*.' + file_extension))
    return found_files


def find_all_files_and_directories_in_a_folder(path_obj):
    if not path_obj.exists():
        raise ValueError('the given Path does not exist!')

    only_all_files = sorted(f for f in path_obj.iterdir() if f.is_file())
    only_all_dirs = sorted(f for f in path_obj.iterdir() if f.is_dir())
    return only_all_files, only_all_dirs



