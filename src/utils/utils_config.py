import yaml


def load_config_file(config_file_path, print_params=True):
    print('\n read YAML file... \n')

    with open(config_file_path) as file:
        config_params = yaml.load(file, Loader=yaml.FullLoader)

    if print_params:
        for idx, elem in enumerate(config_params.items()):
            print(f'key-value pair {idx:03}:  {elem}')
    print('\n')
    return config_params


def main():
    path_to_config_file = 'configurations.yaml'    # path to configuration file
    load_config_file(path_to_config_file, print_params=True)


if __name__=='__main__':
    main()
